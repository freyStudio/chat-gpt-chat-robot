# pyright: reportUnusedImport=false

from fastapi import FastAPI

import openai
import json
import re

# 配置 OpenAI API Key
openai.api_key = "YOU KEY"
app = FastAPI()

# 创建聊天机器人函数


def chat(prompt):
    # 设置请求参数
    prompt = f"对话开始：\n用户: {prompt}\nAI:"
    completions = openai.Completion.create(
        engine="text-davinci-002",
        prompt=prompt,
        max_tokens=1024,
        n=1,
        stop=["\n"],  # 从第一个换行分割，否则会带上其他很多乱七八糟的数据
        temperature=0.5,
    )

    # 解析回复消息
    message = completions.choices[0].text
    # 过滤掉了非中文、数字、字母、标点符号以外的字符
    message = re.sub('[^0-9a-zA-Z\u4e00-\u9fa5\.?,!，。？]+', '', message).strip()
    return message

# API 路由


@app.get("/chat")
async def chat_api(uname: str, content: str):
    print('用户：', uname)
    print('内容', content)
    re_content = chat(content)
    print(re_content)
    return re_content
